const fs = require('fs');
const path = require('path');


//1. Retrieve data for ids : [2, 13, 23].
function retrieveData(fileName, ids) {
    fs.readFile(path.join(__dirname, fileName), (err, data) => {
        if (err) {
            throw new Error(err);
        } else {
            data = JSON.parse(data)

            let result = Object.entries(data.employees).filter((employee) => {
                return ids.includes(employee[1].id);
            }).map((employee) => {
                return employee[1];
            })

            fs.writeFile(path.join(__dirname, "retrive.json"), JSON.stringify(result), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })

        }
    })
}



//2. Group data based on companies.
//{ "Scooby Doo": [], "Powerpuff Brigade": [], "X-Men": []}
function groupBasedOnCompanies(fileName) {
    fs.readFile(path.join(__dirname, fileName), (err, data) => {
        if (err) {
            throw new Error(err);
        } else {
            data = JSON.parse(data)
            let result = Object.entries(data.employees).reduce((acc, employee)=>{
                
                if(acc[employee[1].company]){
                    acc[employee[1].company] = [{...employee[1]}, employee[1]]
                }else{
                    acc[employee[1].company] = employee[1];
                }
                return acc;

            },{});

            fs.writeFile(
                path.join(__dirname, "groupCompany.json"), 
                JSON.stringify(result), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })
        }

    })
}

// groupBasedOnCompanies("data.json");\


// 3. Get all data for company Powerpuff Brigade
function filterBasedOnCompany(fileName, companyName){
    fs.readFile(path.join(__dirname, fileName), (err, data) => {
        if (err) {
            throw new Error(err);
        } else {
            data = JSON.parse(data)

            let result = Object.entries(data.employees).filter((employee) => {
                return companyName.includes(employee[1].company);
            }).map((employee) => {
                return employee[1];
            })

            fs.writeFile(path.join(__dirname, "filterCompany.json"), JSON.stringify(result), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })

        }
    })
}



//  4. Remove entry with id 2.
function removeId(fileName, id){
    fs.readFile(path.join(__dirname, fileName), (err, data) => {
        if (err) {
            throw new Error(err);
        } else {
            data = JSON.parse(data)

            let result = Object.entries(data.employees).filter((employee) => {
                return id!=employee[1].id;
            }).map((employee) => {
                return employee[1];
            })

            fs.writeFile(path.join(__dirname, "removeId.json"), JSON.stringify(result), (err) => {
                if (err) {
                    throw new Error(err);
                }
            })

        }
    })
}
